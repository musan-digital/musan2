Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

# config valid only for Capistrano 3.1
# lock '3.1.0'

set :application, 'motor'
set :repo_url, 'git@bitbucket.org:musan-digital/patashev.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/www/potashev'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{.env}

# Default value for linked_dirs is []
set :linked_dirs, %w{runtime public/assets}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

    desc "composer install"
    task :composer_install do
        on roles(:app) do
             execute "cd #{release_path}; composer install --no-dev --no-interaction --optimize-autoloader --no-progress"
        end
    end

        desc "Migrate DB"
        task :migrate do
            on roles(:app) do
                 execute "cd #{release_path}; ./yii migrate --interactive=0"
            end
        end

    desc "Reload php-fpm"
    task :reload_fpm do
      on roles(:app) do
        execute "sudo service php-fpm reload"
        execute "cd #{release_path}; rm -rf runtime/config/*"
      end
    end

     before :updated, 'deploy:composer_install'
     before :updated, 'deploy:migrate'
     after :finished, 'deploy:reload_fpm'

end
