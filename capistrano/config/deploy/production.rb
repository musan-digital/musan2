# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
role :app, %w{deployer@89.218.72.51}

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server
# definition into the server list. The second argument
# something that quacks like a hash can be used to set
# extended properties on the server.
# server '89.218.72.51', user: 'deployer', roles: %w{app}, port: 22115


server '89.218.72.51',
 user: 'deployer',
 roles: %w{app},
 ssh_options: {
   user: 'deployer', # overrides user setting above
   keys: %w(~/.ssh/id_rsa),
   forward_agent: true,
   auth_methods: %w(publickey password),
   port: 22115
   # password: 'please use keys'
 }
