<?php

return [
    'identityClass' => Da\User\Model\User::class,
    'enableAutoLogin' => true,
];
