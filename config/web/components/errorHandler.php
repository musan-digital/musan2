<?php

return [
    'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',
    'errorAction' => 'site/error',
];
