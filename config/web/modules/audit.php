<?php
return [
    'class' => \bedezign\yii2\audit\Audit::class,
    'layout' => '@app/views/layouts/admin',
    'accessRoles' => ['admin'],
    'ignoreActions' => ['audit/*', 'debug/*'],
];
