<?php
return [
    'class' => Da\User\Module::class,
    'enableRegistration' => false,
    'layout' => '@app/views/layouts/admin',
    'administrators' => ['mitrii'],
];
