'use strict';
var gulp = require('gulp'),
    pngquant = require('imagemin-pngquant');

var gulpLoadPlugins = require('gulp-load-plugins'),
    plugins = gulpLoadPlugins();



/*Пути к файлам ptv*/
var ptv = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        js: 'app/assets/build/js/',
        css: 'app/assets/build/css/',
        img: 'app/assets/build/img/',
        fonts: 'app/assets/build/fonts/'
    },
    src: { //Пути откуда брать исходники
        js: 'app/assets/src/js/ptv.js',//В стилях и скриптах нам понадобятся только main файлы
        style: 'app/assets/src/scss/ptv.scss',
        img: 'app/assets/src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
        fonts: 'app/assets/src/fonts/**/*.*'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        js: 'app/assets/src/js/**/*.js',
        style: 'app/assets/src/scss/**/*.scss',
        img: 'app/assets/src/img/**/*.*',
        fonts: 'app/assets/src/fonts/**/*.*'
    }
};

/*Собираем javascript*/
gulp.task('js_ptv:build', function () {
    gulp.src(ptv.src.js)
        .pipe(plugins.plumber())
        .pipe(plugins.rigger())
        .pipe(plugins.uglify())
        .pipe(gulp.dest(ptv.build.js))
        .pipe(plugins.filesize())
        .on('error', plugins.util.log)
});
/*Собираем стили*/
gulp.task('style_ptv:build', function () {
    gulp.src(ptv.src.style)
        .pipe(plugins.plumber())
        .pipe(plugins.sass())
        .pipe(plugins.autoprefixer())
        .pipe(plugins.minifyCss())
        .pipe(gulp.dest(ptv.build.css))
        .pipe(plugins.filesize())
        .on('error', plugins.util.log)
});
/*Собираем картинки*/
gulp.task('image_ptv:build', function () {
    gulp.src(ptv.src.img)
        .pipe(plugins.imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(ptv.build.img));
});
/*Шрифты*/
gulp.task('fonts_ptv:build', function() {
    gulp.src(ptv.src.fonts)
        .pipe(gulp.dest(ptv.build.fonts))
});

/*Одноразовая сборка*/
gulp.task('build_ptv', [
    'js_ptv:build',
    'style_ptv:build',
    'image_ptv:build',
    'fonts_ptv:build'
]);
/*Следим за изменениями*/
gulp.task('watch_ptv', function(){
    plugins.watch([ptv.watch.style], function(event, cb) {
        gulp.start('style_ptv:build');
    });
    plugins.watch([ptv.watch.js], function(event, cb) {
        gulp.start('js_ptv:build');
    });
    plugins.watch([ptv.watch.img], function(event, cb) {
        gulp.start('image_ptv:build');
    });
    plugins.watch([ptv.watch.fonts], function(event, cb) {
        gulp.start('fonts_ptv:build');
    });
});
/*############### ptv #################*/


/*Финишь запуск всего*/
gulp.task('default', [
    'build_ptv','watch_ptv',
]);


